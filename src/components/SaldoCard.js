import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchSaldo } from "../redux/saldoSlice";
import formatRupiah from "../utils/formatRupiah";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import bgImage from "../assets/images/Background Saldo.png"; // Import your background image

const SaldoCard = () => {
 const dispatch = useDispatch();
 const [showBalance, setShowBalance] = React.useState(false);
 const data = useSelector((state) => state?.saldo?.data);

 const togglePasswordVisibility = () => {
  setShowBalance(!showBalance);
 };

 const containerStyle = {
  backgroundImage: `url(${bgImage})`,
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  backgroundSize: "cover",
 };

 React.useEffect(() => {
  dispatch(fetchSaldo());
 }, [dispatch]);

 return (
  <div className='w-full'>
   <div style={containerStyle} className='text-white p-4 rounded-xl'>
    <p>Your balance</p>
    <div className='flex items-center'>
     <h1 className='font-bold antialiased text-2xl mt-3'>
      <input
       type={showBalance ? "text" : "password"}
       value={formatRupiah(data?.balance === null ? "Loading" : data?.balance)}
       className='bg-transparent border-none'
       disabled
      />
      <div className='flex items-center gap-3 text-sm mt-4 cursor-pointer'>
       <p onClick={togglePasswordVisibility}>
        {!showBalance ? "Show balance" : "Hide balance"}
       </p>
       {showBalance ? (
        <FaEyeSlash
         className='cursor-pointer mt-1'
         onClick={togglePasswordVisibility}
        />
       ) : (
        <FaEye
         className='cursor-pointer mt-1'
         onClick={togglePasswordVisibility}
        />
       )}
      </div>
     </h1>
    </div>
   </div>
  </div>
 );
};

export default SaldoCard;
