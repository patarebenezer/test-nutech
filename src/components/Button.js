const Button = ({ loading, text, disabled }) => {
 return (
  <button
   disabled={disabled}
   type='submit'
   className={`w-full my-4 text-white font-bold btn btn-error ${
    text.includes("Edit") && "btn-outline"
   }`}
  >
   {loading === "loading" ? (
    <>
     <span className='loading loading-spinner'></span>
     loading
    </>
   ) : (
    text
   )}
  </button>
 );
};

export default Button;
