import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProfile } from "../redux/profileSlice";
import userImage from "../assets/images/Profile Photo.png";

const ProfileCard = () => {
 const dispatch = useDispatch();
 const data = useSelector((state) => state?.profile?.data);

 React.useEffect(() => {
  dispatch(fetchProfile());
 }, [dispatch]);

 return (
  <div className='w-full'>
   <img src={userImage} alt='' />
   <p>Selamat datang,</p>
   <h1 className='font-bold text-3xl'>
    {data?.last_name === "" ? "Loading" : data?.last_name}
   </h1>
  </div>
 );
};

export default ProfileCard;
