import React, { useState, useEffect } from "react";

function DateTimeComponent({ datetime }) {
 const [dateObject, setDateObject] = useState(new Date(datetime));

 useEffect(() => {
  setDateObject(new Date(datetime));
 }, [datetime]);

 const tanggal = dateObject.getDate();
 const bulanNames = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
 ];
 const bulanIndex = dateObject.getMonth();
 const bulan = bulanNames[bulanIndex];
 const tahun = dateObject.getFullYear();
 const jam = dateObject.getHours();
 const menit = dateObject.getMinutes();

 return (
  <div>
   <p>
    {tanggal} {bulan} {tahun} {jam}:{menit} WIB
   </p>
  </div>
 );
}

export default DateTimeComponent;
