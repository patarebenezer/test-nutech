import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { toggleSidebar } from "../redux/sidebarSlice";
import { CiMenuBurger } from "react-icons/ci";
import DashboardMenu from "../pages/DashboardPage/components/DashboardMenu";
import { RiTwitterXLine } from "react-icons/ri";
import logo from "../assets/images/Logo.png";

const Header = () => {
 const isSidebarOpen = useSelector((state) => state.sidebar.isSidebarOpen);
 const dispatch = useDispatch();

 const iconStyle = {
  width: "20px",
  height: "20px",
 };

 return (
  <>
   <div className='w-full navbar border-b-2 lg:w-10/12 lg:mx-auto lg:mt-5'>
    <div className='flex-1 mx-2 gap-2'>
     <img src={logo} alt='logo gambar' />
     <h1 className='font-bold antialiased'>SIMS PPOB</h1>
    </div>
    <div className='lg:hidden'>
     <label
      htmlFor='my-drawer-3'
      aria-label='open sidebar'
      className='btn btn-square btn-ghost'
      onClick={() => dispatch(toggleSidebar())}
     >
      {isSidebarOpen ? (
       <RiTwitterXLine style={iconStyle} />
      ) : (
       <CiMenuBurger style={iconStyle} />
      )}
     </label>
    </div>
    <div className='flex-none hidden lg:block'>
     <ul className='menu menu-horizontal font-bold antialiased'>
      <DashboardMenu />
     </ul>
    </div>
   </div>
  </>
 );
};

export default Header;
