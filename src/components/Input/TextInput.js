import { IoMailOutline, IoPersonOutline } from "react-icons/io5";

const TextInput = ({ register, type, errors }) => {
 const tipe = type.replace(/_/g, "");
 return (
  <div className='relative my-4'>
   <input
    type={"text"}
    placeholder={`Enter your ${tipe}`}
    className='w-full py-2 pl-10 pr-4 rounded border focus:outline-none focus:ring focus:border-blue-300'
    {...register(type, { required: true })}
   />
   <div className='absolute left-0 flex items-center ml-3 top-3.5'>
    {type !== "email" ? <IoPersonOutline /> : <IoMailOutline />}
   </div>
   {errors && (
    <span className='text-red-500 capitalize'>{tipe} is required</span>
   )}
  </div>
 );
};

export default TextInput;
