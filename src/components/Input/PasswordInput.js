import React from "react";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import { BiLockAlt } from "react-icons/bi";

const PasswordInput = ({ errors, register, type }) => {
 const [showPassword, setShowPassword] = React.useState(false);

 const togglePasswordVisibility = () => {
  setShowPassword(!showPassword);
 };

 return (
  <div className='relative my-4'>
   <input
    type={showPassword ? "text" : "password"}
    placeholder={
     type.includes("confirm") ? "Confirm your password" : "Enter your password"
    }
    {...register(type, { required: true })}
    className='w-full py-2 pl-10 pr-4 rounded border focus:outline-none focus:ring focus:border-blue-300'
   />
   <div className='absolute left-0 flex items-center ml-3 top-3.5'>
    <BiLockAlt />
   </div>
   <div className='absolute right-0 flex items-center mr-3 top-3.5'>
    {showPassword ? (
     <FaEyeSlash
      className='text-gray-500 cursor-pointer'
      onClick={togglePasswordVisibility}
     />
    ) : (
     <FaEye
      className='text-gray-500 cursor-pointer'
      onClick={togglePasswordVisibility}
     />
    )}
   </div>
   {errors.password && (
    <span className='text-red-500 capitalize'>Password is required</span>
   )}
  </div>
 );
};

export default PasswordInput;
