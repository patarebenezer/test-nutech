import { createBrowserRouter } from "react-router-dom";
import DashboardPage from "../pages/DashboardPage";
import TopupPage from "../pages/TopupPage";
import AuthPage from "../pages/AuthPage";
import ErrorPage from "../pages/ErrorPage";
import PrivateRoute from "../PrivateRoute";
import ProfilePage from "../pages/ProfilePage";
import TransactionPage from "../pages/TransactionPage";
import TransactionDetail from "../pages/TransactionPage/TransactionDetail";

const router = createBrowserRouter([
 {
  path: "/",
  element: (
   <PrivateRoute>
    <AuthPage />
   </PrivateRoute>
  ),
 },
 {
  path: "/dashboard",
  element: (
   <PrivateRoute>
    <DashboardPage />
   </PrivateRoute>
  ),
 },
 {
  path: "/dashboard/topup",
  element: (
   <PrivateRoute>
    <TopupPage />
   </PrivateRoute>
  ),
 },
 {
  path: "/dashboard/akun",
  element: (
   <PrivateRoute>
    <ProfilePage />
   </PrivateRoute>
  ),
 },
 {
  path: "/dashboard/transaksi",
  element: (
   <PrivateRoute>
    <TransactionPage />
   </PrivateRoute>
  ),
 },
 {
  path: "/dashboard/transaksi/:id",
  element: (
   <PrivateRoute>
    <TransactionDetail />
   </PrivateRoute>
  ),
 },
 {
  path: "/auth",
  element: (
   <PrivateRoute>
    <AuthPage />
   </PrivateRoute>
  ),
 },
 {
  path: "/*",
  element: <ErrorPage />,
 },
]);

export default router;
