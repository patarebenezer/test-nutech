import React from "react";
import "./App.css";
import router from "./routes";
import { RouterProvider } from "react-router-dom";
function App() {
 React.useEffect(() => {
  document.title = "SIMS PPOB-Patar Siahaan";
 }, []);
 return (
  <>
   <RouterProvider router={router} />
  </>
 );
}

export default App;
