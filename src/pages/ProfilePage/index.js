import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import { IoMailOutline, IoPersonOutline } from "react-icons/io5";
import { CiEdit } from "react-icons/ci";
import { useNavigate } from "react-router";
import { fetchProfile, postProfileImage } from "../../redux/profileSlice";
import Button from "../../components/Button";
import Header from "../../components/Header";
import SuccessAlert from "../../components/SuccessAlert";

const ProfilePage = () => {
 const dispatch = useDispatch();
 const navigate = useNavigate();
 const profile = useSelector((state) => state?.profile?.data);

 const [edit, setEdit] = useState("Edit Profile");
 const [status, setStatus] = useState("");
 const [selectedImage, setSelectedImage] = useState(null);

 const {
  register,
  handleSubmit,
  formState: { errors },
  setValue,
 } = useForm();

 const onSubmit = async (data) => {
  if (selectedImage) {
   dispatch(postProfileImage(selectedImage));
  }
  if (edit.includes("Profile")) {
   setEdit("Simpan");
  }
  if (edit.includes("Simpan")) {
   const response = await dispatch(
    fetchProfile({
     first_name: data.first_name,
     last_name: data.last_name,
    })
   );
   setStatus("succeeded");
   setEdit("Edit Profile");
   setTimeout(() => {
    setStatus("");
   }, 3000);
   return response;
  }
 };

 const logout = () => {
  localStorage.removeItem("token");
  navigate("/");
  setEdit(false);
 };

 const handleImageChange = (e) => {
  const file = e.target.files[0];
  setSelectedImage(file);
 };

 useEffect(() => {
  dispatch(fetchProfile());
 }, [dispatch]);

 const renderInput = (label, name, icon) => (
  <div className='relative'>
   <label className='font-bold text-sm'>{label}</label>
   <input
    type='text'
    className='w-full py-2 mb-4 pl-10 pr-4 rounded border focus:outline-none focus:ring focus:border-blue-300'
    {...register(name, { required: name === "email" ? false : true })}
    defaultValue={profile[name]}
    onChange={(e) => setValue(name, e.target.value)}
    disabled={name === "email" ? true : !edit}
   />
   <div className='absolute left-0 flex items-center ml-3 top-9'>{icon}</div>
   {errors[name] && (
    <span className='text-red-500 capitalize'>{label} is required</span>
   )}
  </div>
 );

 return (
  <>
   <Header />
   <div className='w-10/12 lg:w-1/2 mx-auto mt-8'>
    <form onSubmit={handleSubmit(onSubmit)}>
     <div className='flex justify-center my-10'>
      <div className='relative'>
       <img src={profile.profile_image} alt='profil' className='w-28' />
       <div className='absolute bottom-0 right-0 rounded-full border p-1 bg-white hover:bg-slate-100 cursor-pointer'>
        <input
         type='file'
         accept='image/jpeg, image/png'
         onChange={handleImageChange}
         id='fileInput'
         style={{ position: "absolute", top: "-9999px" }}
        />
        <label htmlFor='fileInput' className='cursor-pointer'>
         <CiEdit />
        </label>
       </div>
      </div>
     </div>
     {status === "succeeded" && (
      <SuccessAlert
       text={status === "succeeded" && "Successfully changed profile"}
      />
     )}
     {renderInput("Email", "email", <IoMailOutline />)}
     {renderInput("Nama Depan", "first_name", <IoPersonOutline />)}
     {renderInput("Nama Belakang", "last_name", <IoPersonOutline />)}
     <Button text={edit} />
    </form>
    <div onClick={logout} className={edit.includes("Simpan") ? "hidden" : ""}>
     <Button text={"Logout"} />
    </div>
   </div>
  </>
 );
};

export default ProfilePage;
