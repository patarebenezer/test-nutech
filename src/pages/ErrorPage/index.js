import { useEffect, useRef } from "react";
import "./ErrorPage.css";

const ErrorPage = () => {
 const canvasRef = useRef(null);
 const ctxRef = useRef(null);
 const imgDataRef = useRef(null);
 const WIDTH = 700;
 const HEIGHT = 500;
 const flickerIntervalRef = useRef(null);

 useEffect(() => {
  const canvas = canvasRef.current;
  const ctx = canvas.getContext("2d");
  ctxRef.current = ctx;

  canvas.width = WIDTH;
  canvas.height = HEIGHT;
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, WIDTH, HEIGHT);
  ctx.fill();
  imgDataRef.current = ctx.getImageData(0, 0, WIDTH, HEIGHT);

  flickerIntervalRef.current = setInterval(flickering, 30);

  return () => {
   clearInterval(flickerIntervalRef.current);
  };
 }, []);

 const flickering = () => {
  const ctx = ctxRef.current;
  const imgData = imgDataRef.current;
  const pix = imgData.data;

  for (let i = 0; i < pix.length; i += 4) {
   const color = Math.random() * 255 + 50;
   pix[i] = color;
   pix[i + 1] = color;
   pix[i + 2] = color;
  }

  ctx.putImageData(imgData, 0, 0);
 };
 return (
  <>
   <h1 className='h1'>404</h1>
   <div class='frame'>
    <div></div>
    <div></div>
    <div></div>
   </div>
   <div class='caps'>
    <img src='http://ademilter.com/caps.png' alt='' />
   </div>
   <canvas ref={canvasRef} id='canvas'></canvas>
  </>
 );
};

export default ErrorPage;
