import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { toggleSidebar } from "../../redux/sidebarSlice";
import { postTopup } from "../../redux/topupSlice";
import Header from "../../components/Header";
import SuccessAlert from "../../components/SuccessAlert";
import DashboardUser from "../DashboardPage/components/DashboardUser";
import DashboardSidebar from "../DashboardPage/components/DashboardSidebar";
import { HiOutlineCurrencyDollar } from "react-icons/hi2";

const TopupPage = () => {
 const dispatch = useDispatch();
 const isSidebarOpen = useSelector((state) => state.sidebar.isSidebarOpen);
 const topupStatus = useSelector((state) => state?.topup?.status);
 const topupError = useSelector((state) => state?.topup?.error);

 const [topupAmount, setTopupAmount] = React.useState("");
 const [statusForm, setStatusForm] = React.useState("");
 const [isButtonDisabled, setIsButtonDisabled] = React.useState(true);
 const labels = [
  "Rp10.000",
  "Rp20.000",
  "Rp50.000",
  "Rp100.000",
  "Rp250.000",
  "Rp500.000",
 ];

 const handleInputChange = (e) => {
  const inputValue = e.target.value;
  setTopupAmount(inputValue);

  const isValidInput =
   /^\d+$/.test(inputValue) && inputValue >= 10000 && inputValue <= 1000000;
  setIsButtonDisabled(!isValidInput);
 };

 const handleSelectTopup = (label) => {
  const numericValue = parseInt(label.replace(/\D+/g, ""), 10);
  setTopupAmount(numericValue.toString());
  handleInputChange({ target: { value: numericValue.toString() } });
 };

 const handleTopup = () => {
  dispatch(postTopup({ top_up_amount: topupAmount }));
  setStatusForm(topupStatus === "succeeded" && "Top Up Balance berhasil");
 };

 React.useEffect(() => {
  setTimeout(() => {
   setStatusForm("");
   setTopupAmount("");
  }, 4000);
 }, [topupStatus]);

 return (
  <div>
   <Header />
   <DashboardUser />
   <DashboardSidebar
    isSidebarOpen={isSidebarOpen}
    toggleSidebar={() => dispatch(toggleSidebar())}
   />
   {topupError && (
    <div className='text-red-500 text-center font-semibold antialiased'>
     {topupError}
    </div>
   )}
   {statusForm && <SuccessAlert text={"Top up berhasil"} />}
   <div className='flex flex-col gap-4 mt-8 md:flex-row md:justify-center px-5'>
    <div className='w-full md:w-1/2 relative'>
     <input
      type='text'
      placeholder='Masukkan nominal Top up'
      className='w-full py-2 pl-10 pr-4 rounded border focus:outline-none focus:ring focus:border-blue-300'
      value={topupAmount}
      onChange={handleInputChange}
     />
     <div className='absolute left-0 flex items-center ml-3 top-3.5'>
      <HiOutlineCurrencyDollar />
     </div>
     <button
      className='mt-6 btn bg-red-500 hover:bg-red-600 text-white w-full'
      onClick={handleTopup}
      disabled={isButtonDisabled}
     >
      {topupStatus === "loading" ? (
       <>
        <span className='loading loading-spinner'></span>
        Loading
       </>
      ) : (
       "Masuk"
      )}
     </button>
    </div>
    <div className='grid grid-cols-3 gap-4 w-full md:w-1/3'>
     {labels.map((label, index) => (
      <input
       onClick={() => handleSelectTopup(label)}
       key={index}
       className='join-item btn'
       type='radio'
       name='options'
       aria-label={label}
      />
     ))}
    </div>
   </div>
  </div>
 );
};

export default TopupPage;
