import DateTimeComponent from "../../components/DateTimeComponent";
import formatRupiah from "../../utils/formatRupiah";

const TransactionCard = ({
 key,
 amount,
 data_date,
 desc,
 transaction_type,
}) => {
 return (
  <div
   className='card w-full lg:w-10/12 lg:mx-auto bg-base-100 shadow-md my-3'
   key={key}
  >
   <div className='card-body'>
    <h2
     className={`card-title ${
      transaction_type === "TOPUP" ? "text-green-500" : "text-red-500"
     }`}
    >
     {transaction_type.includes("TOPUP") ? "+ " : "- "}
     {formatRupiah(amount)}
    </h2>
    <p className='text-gray-400'>{desc}</p>
    <div className='card-actions justify-end text-slate-500 font-semibold'>
     <DateTimeComponent datetime={data_date} />
    </div>
   </div>
  </div>
 );
};

export default TransactionCard;
