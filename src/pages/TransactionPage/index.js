import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../components/Header";
import { fetchTransaction } from "../../redux/transactionSlice";
import DashboardUser from "../DashboardPage/components/DashboardUser";
import TransactionCard from "./TransactionCard";

const TransactionPage = () => {
 const dispatch = useDispatch();
 const transaction = useSelector((state) => state?.transaction?.data);

 const [limit, setLimit] = useState(5);
 const [loading, setLoading] = useState(false);

 const contentStyle = {
  display: "flex",
  flexDirection: "column",
  height: "100vh",
  overflowY: "auto",
 };

 const loadMoreTransactions = () => {
  if (!loading) {
   setLoading(true);
   setLimit((prevLimit) => prevLimit + 3);
   dispatch(fetchTransaction({ offset: 0, limit: limit + 3 })).then(() => {
    setLoading(false);
   });
  }
 };

 useEffect(() => {
  dispatch(fetchTransaction({ offset: 0, limit }));
 }, [dispatch, limit]);

 return (
  <div style={contentStyle} className='mx-4'>
   <Header />
   <DashboardUser />
   {transaction?.records?.map((item, index) => (
    <TransactionCard
     key={index}
     amount={item.total_amount}
     data_date={item.created_on}
     desc={item.description}
     transaction_type={item.transaction_type}
    />
   ))}
   <p
    className='text-red-500 hover:underline text-md text-center cursor-pointer my-8'
    onClick={loadMoreTransactions}
   >
    {loading ? (
     <button className='btn'>
      <span className='loading loading-spinner'></span>
      loading
     </button>
    ) : (
     <button className='btn btn-error'>Show more</button>
    )}
   </p>
  </div>
 );
};

export default TransactionPage;
