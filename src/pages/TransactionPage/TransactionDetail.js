import React, { useState, useEffect } from "react";
import Header from "../../components/Header";
import DashboardUser from "../DashboardPage/components/DashboardUser";
import { useNavigate, useParams } from "react-router";
import { CiShoppingCart } from "react-icons/ci";
import { useDispatch, useSelector } from "react-redux";
import formatRupiah from "../../utils/formatRupiah";
import { HiOutlineCurrencyDollar } from "react-icons/hi2";
import Button from "../../components/Button";
import { postTransaction } from "../../redux/transactionSlice";
import SuccessAlert from "../../components/SuccessAlert";

const TransactionDetail = () => {
 const { id } = useParams();
 const dispatch = useDispatch();
 const navigate = useNavigate();
 const services = useSelector((state) => state.services.data);
 const statusTrx = useSelector((state) => state.transaction.status);
 const [amount, setAmount] = useState("");
 const [status, setStatus] = useState("");

 const payTransaction = async () => {
  const response = await dispatch(
   postTransaction({
    service_code: id,
   })
  );
  response && setStatus("Payment Success");
  setTimeout(() => {
   navigate("/dashboard/transaksi");
   setStatus("");
  }, [3000]);
  return response;
 };

 useEffect(() => {
  const filteredService = services.find(
   (service) => service.service_code === id
  );

  if (filteredService) {
   setAmount(filteredService.service_tariff);
  } else {
   setAmount("");
  }
 }, [id, services]);

 return (
  <div>
   <Header />
   <DashboardUser />
   <div>
    {status && <SuccessAlert text={status} />}
    <div className='w-11/12 lg:w-10/12 mx-auto flex items-center gap-2 underline'>
     <CiShoppingCart />
     <span className='text-sm font-semibold text-gray-500 antialiased'>
      Pembayaran {id}
     </span>
    </div>
   </div>
   <div className='relative w-11/12 lg:w-10/12 mx-auto my-7'>
    <input
     type='text'
     className='w-full py-2 pl-10 pr-4 rounded border focus:outline-none focus:ring focus:border-blue-300'
     value={formatRupiah(amount)}
     readOnly
    />
    <div className='absolute left-0 flex items-center ml-3 top-3.5'>
     <HiOutlineCurrencyDollar />
    </div>
    <div onClick={payTransaction}>
     <Button loading={statusTrx} text={"Bayar"} disabled={status.length > 1} />
    </div>
   </div>
  </div>
 );
};

export default TransactionDetail;
