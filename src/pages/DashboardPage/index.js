import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { toggleSidebar } from "../../redux/sidebarSlice";
import Header from "../../components/Header";
import DashboardUser from "./components/DashboardUser";
import DashboardSidebar from "./components/DashboardSidebar";
import DashboardContent from "./components/DashboardContent";

const DashboardPage = () => {
 const dispatch = useDispatch();
 const isSidebarOpen = useSelector((state) => state.sidebar.isSidebarOpen);

 const contentStyle = {
  display: "flex",
  flexDirection: "column",
  height: "100vh",
  overflowY: "auto",
 };

 return (
  <>
   <div className='drawer-content flex flex-col flex-1' style={contentStyle}>
    <Header />
    <DashboardUser />
    <DashboardContent />
   </div>

   <DashboardSidebar
    isSidebarOpen={isSidebarOpen}
    toggleSidebar={() => dispatch(toggleSidebar())}
   />
  </>
 );
};

export default DashboardPage;
