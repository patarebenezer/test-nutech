import ProfileCard from "../../../components/ProfileCard";
import SaldoCard from "../../../components/SaldoCard";

const DashboardUser = () => {
 return (
  <div className='w-full lg:w-10/12 lg:mx-auto flex flex-wrap px-4 py-8'>
   <div className='w-full sm:w-1/2 md:w-1/3 lg:w-full xl:w-1/2 p-2'>
    <ProfileCard />
   </div>
   <div className='w-full sm:w-1/2 md:w-2/3 lg:w-full xl:w-1/2 p-2'>
    <SaldoCard />
   </div>
  </div>
 );
};

export default DashboardUser;
