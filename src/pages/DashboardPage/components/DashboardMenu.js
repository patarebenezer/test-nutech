import React from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { toggleSidebar } from "../../../redux/sidebarSlice";
import { CiDark, CiLight } from "react-icons/ci";

const DashboardMenu = () => {
 const dispatch = useDispatch();

 const [theme, setTheme] = React.useState("light");
 const toggleTheme = () => {
  setTheme(theme === "dark" ? "light" : "dark");
 };

 React.useEffect(() => {
  document.querySelector("html").setAttribute("data-theme", theme);
 }, [theme]);

 const iconStyle = {
  width: "20px",
  height: "20px",
 };
 return (
  <>
   <li>
    <label className='swap swap-rotate'>
     <input onClick={toggleTheme} type='checkbox' />
     <div className='swap-on'>
      <CiDark style={iconStyle} />
     </div>
     <div className='swap-off'>
      <CiLight style={iconStyle} />
     </div>
    </label>
   </li>
   <li>
    <Link to='/dashboard' onClick={() => dispatch(toggleSidebar())}>
     Dashboard
    </Link>
   </li>
   <li>
    <Link to='/dashboard/topup' onClick={() => dispatch(toggleSidebar())}>
     Topup
    </Link>
   </li>
   <li>
    <Link to='/dashboard/transaksi'>Transaksi</Link>
   </li>
   <li>
    <Link to='/dashboard/akun' onClick={() => dispatch(toggleSidebar())}>
     Akun
    </Link>
   </li>
  </>
 );
};

export default DashboardMenu;
