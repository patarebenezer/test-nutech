import DashboardProduct from "./DashboardProduct";
import DashboardBanners from "./DashboardBanners";

const DashboardContent = () => {
 return (
  <div>
   <DashboardProduct />
   <DashboardBanners />
  </div>
 );
};

export default DashboardContent;
