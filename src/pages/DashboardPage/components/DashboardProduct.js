import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchServices } from "../../../redux/servicesSlice";
import { Link } from "react-router-dom";

const DashboardProduct = () => {
 const dispatch = useDispatch();
 const product = useSelector((state) => state?.services?.data);
 const productList = Array.isArray(product) ? product : [];

 React.useEffect(() => {
  dispatch(fetchServices());
 }, [dispatch]);

 return (
  <div className='grid grid-cols-3 lg:w-10/12 lg:mx-auto sm:grid-cols-4 md:grid-cols-6 lg:flex gap-4 justify-around items-center px-4 my-5'>
   {productList.map((item) => (
    <Link to={`/dashboard/transaksi/${item.service_code}`}>
     <div
      key={item.service_code}
      className='flex flex-col items-center cursor-pointer gap-2'
     >
      <img
       src={item.service_icon}
       alt={item.service_name}
       className='rounded-full'
      />
      <p className='text-center text-sm text-gray-500 font-semibold antialiased'>
       {item.service_name}
      </p>
     </div>
    </Link>
   ))}
  </div>
 );
};

export default DashboardProduct;
