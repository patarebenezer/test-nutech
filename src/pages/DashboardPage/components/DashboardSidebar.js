import DashboardMenu from "./DashboardMenu";

const DashboardSidebar = ({ isSidebarOpen, toggleSidebar }) => {
 return (
  <>
   <input id='my-drawer-3' type='checkbox' className='drawer-toggle hidden' />
   <div className={`drawer-side ${isSidebarOpen ? "lg:block" : "hidden"}`}>
    <label
     htmlFor='my-drawer-3'
     aria-label='close sidebar'
     className='drawer-overlay'
     onClick={toggleSidebar}
    ></label>
    <ul className='menu p-4 w-80 min-h-full bg-base-200'>
     <DashboardMenu />
    </ul>
   </div>
  </>
 );
};

export default DashboardSidebar;
