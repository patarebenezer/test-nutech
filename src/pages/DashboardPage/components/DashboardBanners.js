import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchBanner } from "../../../redux/bannerSlice";

const DashboardBanners = () => {
 const dispatch = useDispatch();
 const banners = useSelector((state) => state?.banner?.data);

 React.useEffect(() => {
  dispatch(fetchBanner());
 }, [dispatch]);

 return (
  <div className='lg:w-10/12 lg:mx-auto'>
   <h3 className='font-bold antialiased mt-16 mb-4 '>Temukan promo menarik</h3>
   <div className='grid sm:grid-cols-2 md:grid-cols-3 lg:flex gap-4 justify-around items-center w-full mb-24'>
    {banners.map((item, index) => (
     <div key={index} className='flex flex-col items-center cursor-pointer'>
      <img src={item.banner_image} alt={item.banner_name} />
     </div>
    ))}
   </div>
  </div>
 );
};

export default DashboardBanners;
