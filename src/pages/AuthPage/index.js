import { useForm } from "react-hook-form";
import { useState } from "react";
import Button from "../../components/Button";
import TextInput from "../../components/Input/TextInput";
import PasswordInput from "../../components/Input/PasswordInput";
import Logo from "../../assets/images/Logo.png";
import backgroundLogin from "../../assets/images/Illustrasi Login.png";
import { useDispatch, useSelector } from "react-redux";
import { login, registration } from "../../redux/authSlice";
import { useNavigate } from "react-router";

const AuthPage = () => {
 const {
  register,
  handleSubmit,
  formState: { errors },
  reset,
 } = useForm();
 const dispatch = useDispatch();
 const authStatus = useSelector((state) => state.auth.status);
 const authError = useSelector((state) => state.auth.error);
 const [statusForm, setStatusForm] = useState(true);

 const isRegistrationPage = !statusForm;
 const navigate = useNavigate();

 const onSubmit = async (data) => {
  const checkPassword =
   data.password === data.confirm_password && data.password;
  if (isRegistrationPage) {
   await dispatch(
    registration({
     email: data.email,
     password: checkPassword,
     first_name: data.first_name,
     last_name: data.last_name,
    })
   );
  } else {
   const response = await dispatch(
    login({ email: data.email, password: data.password })
   );
   localStorage.setItem("token", response.payload.data.token);
   response.payload.data.token && navigate("/dashboard");
   return response;
  }
 };

 const handleAuth = () => {
  setStatusForm(!statusForm);
  reset();
 };

 const commonInputs = (
  <>
   <TextInput register={register} errors={errors.email} type='email' />
   <PasswordInput register={register} errors={errors} type='password' />
  </>
 );

 const registrationInputs = (
  <>
   <TextInput register={register} errors={errors.email} type='email' />
   <TextInput
    register={register}
    errors={errors.first_name}
    type='first_name'
   />
   <TextInput register={register} errors={errors.last_name} type='last_name' />
   <PasswordInput register={register} errors={errors} type='password' />
   <PasswordInput register={register} errors={errors} type='confirm_password' />
  </>
 );

 return (
  <div>
   <div className='flex w-full justify-center items-center h-screen'>
    <form
     className='p-2 lg:p-8 w-full lg:w-11/12'
     onSubmit={handleSubmit(onSubmit)}
    >
     <div className='px-14'>
      <div className='flex justify-center gap-2'>
       <img src={Logo} alt='icon logo' />
       <h1 className='font-semibold text-xl antialiased'>SIMS PPOB</h1>
      </div>
      <h1 className='font-bold text-2xl antialiased mx-auto py-5 text-center w-full lg:w-1/2'>
       {!statusForm
        ? "Masuk atau buat akun untuk memulai"
        : "Lengkapi data untuk membuat akun"}
      </h1>
      {authError && (
       <div className='text-red-500 text-center font-semibold antialiased'>
        {authError}
       </div>
      )}

      {isRegistrationPage ? registrationInputs : commonInputs}
      <Button loading={authStatus} text={"Masuk"} />

      <p className='text-sm text-gray-500 flex gap-1 justify-center'>
       {!statusForm ? "Sudah" : "Belum"} punya akun?{" "}
       <div className='font-bold'>
        {!statusForm ? "Login" : "Register"}
        <span
         className='text-red-500 ml-1 cursor-pointer hover:underline'
         onClick={handleAuth}
        >
         Disini
        </span>
       </div>
      </p>
     </div>
    </form>

    <div className={`w-full hidden lg:block`}>
     <img src={backgroundLogin} alt='background' className='w-full h-auto' />
    </div>
   </div>
  </div>
 );
};

export default AuthPage;
