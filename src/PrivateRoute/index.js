import { Navigate } from "react-router-dom";

const PrivateRoute = ({ children }) => {
 const token = localStorage.getItem("token");
 const currentPathname = window.location.pathname;

 if (token !== null && currentPathname === "/") {
  return <Navigate to='/dashboard' />;
 } else if (token !== null && currentPathname.includes("auth")) {
  return <Navigate to='/dashboard' />;
 } else if (!token && currentPathname.includes("/dashboard")) {
  return <Navigate to='/auth' />;
 }

 return children;
};

export default PrivateRoute;
