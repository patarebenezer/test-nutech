import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
 data: [],
 status: "idle",
 error: null,
};

const BASE_URL = "https://take-home-test-api.nutech-integrasi.app";
const token = localStorage.getItem("token");

export const fetchTransaction = createAsyncThunk(
 "dashboard/fetchTransaction",
 async ({ offset, limit }) => {
  const response = await axios.get(
   `${BASE_URL}/transaction/history/?offset=${offset}&limit=${limit}`,
   {
    headers: { Authorization: `Bearer ${token}` },
   }
  );
  return response.data.data;
 }
);

export const postTransaction = createAsyncThunk(
 "dashboard/postTransaction",
 async (data) => {
  const response = await axios.post(`${BASE_URL}/transaction`, data, {
   headers: { Authorization: `Bearer ${token}` },
  });
  return response.data.data;
 }
);

const transactionSlice = createSlice({
 name: "transaction",
 initialState,
 reducers: {},
 extraReducers: (builder) => {
  builder
   .addCase(fetchTransaction.pending, (state) => {
    state.status = "loading";
   })
   .addCase(fetchTransaction.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(fetchTransaction.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   })
   .addCase(postTransaction.pending, (state) => {
    state.status = "loading";
   })
   .addCase(postTransaction.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(postTransaction.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   });
 },
});

export default transactionSlice.reducer;
