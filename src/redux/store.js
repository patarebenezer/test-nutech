import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./authSlice";
import profileReducer from "./profileSlice";
import saldoReducer from "./saldoSlice";
import servicesReducer from "./servicesSlice";
import bannerReducer from "./bannerSlice";
import sidebarReducer from "./sidebarSlice";
import topupReducer from "./topupSlice";
import transactionReducer from "./transactionSlice";

export const store = configureStore({
 reducer: {
  auth: authReducer,
  profile: profileReducer,
  saldo: saldoReducer,
  services: servicesReducer,
  banner: bannerReducer,
  sidebar: sidebarReducer,
  topup: topupReducer,
  transaction: transactionReducer,
 },
});

export default store;
