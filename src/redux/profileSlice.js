import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
 data: [],
 status: "idle",
 error: null,
};

const BASE_URL = "https://take-home-test-api.nutech-integrasi.app";
const token = localStorage.getItem("token");
const MAX_FILE_SIZE_BYTES = 100 * 1024; // 100KB
const ALLOWED_IMAGE_FORMATS = ["image/jpeg", "image/png"];

export const fetchProfile = createAsyncThunk(
 "dashboard/fetchProfile",
 async () => {
  const response = await axios.get(`${BASE_URL}/profile`, {
   headers: { Authorization: `Bearer ${token}` },
  });
  return response.data.data;
 }
);

export const postProfile = createAsyncThunk(
 "dashboard/postProfile",
 async (data) => {
  const response = await axios.put(`${BASE_URL}/profile/update`, data, {
   headers: { Authorization: `Bearer ${token}` },
  });
  return response.data.data;
 }
);

export const postProfileImage = createAsyncThunk(
 "dashboard/postProfileImage",
 async (file) => {
  if (file.size > MAX_FILE_SIZE_BYTES) {
   throw new Error("File size exceeds the maximum allowed (100KB).");
  }

  if (!ALLOWED_IMAGE_FORMATS.includes(file.type)) {
   throw new Error("Invalid file format. Only JPG and PNG are allowed.");
  }

  const formData = new FormData();
  formData.append("image", file);

  const response = await axios.put(`${BASE_URL}/profile/image`, formData, {
   headers: {
    Authorization: `Bearer ${token}`,
    "Content-Type": "multipart/form-data",
   },
  });

  return response.data.data;
 }
);

const profileSlice = createSlice({
 name: "profile",
 initialState,
 reducers: {},
 extraReducers: (builder) => {
  builder
   .addCase(fetchProfile.pending, (state) => {
    state.status = "loading";
   })
   .addCase(fetchProfile.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(fetchProfile.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   })
   .addCase(postProfile.pending, (state) => {
    state.status = "loading";
   })
   .addCase(postProfile.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(postProfile.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   })
   .addCase(postProfileImage.pending, (state) => {
    state.status = "loading";
   })
   .addCase(postProfileImage.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(postProfileImage.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   });
 },
});

export default profileSlice.reducer;
