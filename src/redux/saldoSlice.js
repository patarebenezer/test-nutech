import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
 data: [],
 status: "idle",
 error: null,
};

const BASE_URL = "https://take-home-test-api.nutech-integrasi.app";
const token = localStorage.getItem("token");

export const fetchSaldo = createAsyncThunk("dashboard/fetchSaldo", async () => {
 const response = await axios.get(`${BASE_URL}/balance`, {
  headers: { Authorization: `Bearer ${token}` },
 });
 return response.data.data;
});

const saldoSlice = createSlice({
 name: "saldo",
 initialState,
 reducers: {},
 extraReducers: (builder) => {
  builder
   .addCase(fetchSaldo.pending, (state) => {
    state.status = "loading";
   })
   .addCase(fetchSaldo.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(fetchSaldo.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   });
 },
});

export default saldoSlice.reducer;
