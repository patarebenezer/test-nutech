import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
 data: [],
 status: "idle",
 error: null,
};

const BASE_URL = "https://take-home-test-api.nutech-integrasi.app";
const token = localStorage.getItem("token");

export const postTopup = createAsyncThunk(
 "dashboard/fetchTopup",
 async (data) => {
  try {
   const response = await axios.post(`${BASE_URL}/topup`, data, {
    headers: { Authorization: `Bearer ${token}` },
   });
   return response.data.data;
  } catch (error) {
   throw error.response.data;
  }
 }
);

const topupSlice = createSlice({
 name: "topup",
 initialState,
 reducers: {},
 extraReducers: (builder) => {
  builder
   .addCase(postTopup.pending, (state) => {
    state.status = "loading";
   })
   .addCase(postTopup.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(postTopup.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   });
 },
});

export default topupSlice.reducer;
