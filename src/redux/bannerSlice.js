import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
 data: [],
 status: "idle",
 error: null,
};

const BASE_URL = "https://take-home-test-api.nutech-integrasi.app";
const token = localStorage.getItem("token");

export const fetchBanner = createAsyncThunk(
 "dashboard/fetchBanner",
 async () => {
  const response = await axios.get(`${BASE_URL}/banner`, {
   headers: { Authorization: `Bearer ${token}` },
  });
  return response.data.data;
 }
);

const bannerSlice = createSlice({
 name: "banner",
 initialState,
 reducers: {},
 extraReducers: (builder) => {
  builder
   .addCase(fetchBanner.pending, (state) => {
    state.status = "loading";
   })
   .addCase(fetchBanner.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(fetchBanner.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   });
 },
});

export default bannerSlice.reducer;
