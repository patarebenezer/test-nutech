import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
 data: [],
 status: "idle",
 error: null,
};

const BASE_URL = "https://take-home-test-api.nutech-integrasi.app";
const token = localStorage.getItem("token");

export const fetchServices = createAsyncThunk(
 "dashboard/fetchServices",
 async () => {
  const response = await axios.get(`${BASE_URL}/services`, {
   headers: { Authorization: `Bearer ${token}` },
  });
  return response.data.data;
 }
);

const servicesSlice = createSlice({
 name: "services",
 initialState,
 reducers: {},
 extraReducers: (builder) => {
  builder
   .addCase(fetchServices.pending, (state) => {
    state.status = "loading";
   })
   .addCase(fetchServices.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(fetchServices.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   });
 },
});

export default servicesSlice.reducer;
