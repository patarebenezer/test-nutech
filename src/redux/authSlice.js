import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
 user: null,
 status: "idle",
 error: null,
};

const BASE_URL = "https://take-home-test-api.nutech-integrasi.app";

const createAsyncThunkWithAPI = (name, url) => {
 return createAsyncThunk(`auth/${name}`, async (credentials) => {
  try {
   const response = await axios.post(url, credentials);
   return response.data;
  } catch (error) {
   throw error.response.data;
  }
 });
};

export const login = createAsyncThunkWithAPI("login", `${BASE_URL}/login`);

export const registration = createAsyncThunkWithAPI(
 "registration",
 `${BASE_URL}/registration`
);

const authSlice = createSlice({
 name: "auth",
 initialState,
 reducers: {},
 extraReducers: (builder) => {
  [login, registration].forEach((asyncThunk) => {
   builder
    .addCase(asyncThunk.pending, (state) => {
     state.status = "loading";
    })
    .addCase(asyncThunk.fulfilled, (state, action) => {
     state.status = "succeeded";
     state.user = action.payload;
     state.error = null;
    })
    .addCase(asyncThunk.rejected, (state, action) => {
     state.status = "failed";
     state.error = action.error.message;
    });
  });
 },
});

export default authSlice.reducer;
