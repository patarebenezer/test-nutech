function formatRupiah(angka) {
 if (typeof angka !== "number") {
  return angka;
 }

 const formatter = new Intl.NumberFormat("id-ID", {
  style: "currency",
  currency: "IDR",
  minimumFractionDigits: 0,
  maximumFractionDigits: 0,
 });

 return formatter.format(angka);
}

export default formatRupiah;
